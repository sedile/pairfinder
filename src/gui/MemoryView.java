package gui;

import ausnahmen.FeldException;
import javafx.event.EventHandler;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;

/**
 * Diese Klasse zeichnet das aktuelle Spielgeschehen
 * @author Sebastian
 */
public class MemoryView extends Canvas {
	
	private MemoryFrame mf;
	private GraphicsContext gc;
	private final byte ZELLE = Byte.MAX_VALUE;
	
	/**
	 * Der Konstruktor erzeugt eine Canvas was das Spielfeld darstellt
	 * @param memoryframe Hauptfenster
	 */
	public MemoryView(MemoryFrame memoryframe) {
		mf = memoryframe;
		gc = getGraphicsContext2D();
		setWidth(mf.getKontroller().getFeldmax() * ZELLE);
		setHeight(mf.getKontroller().getFeldmax() * ZELLE);
		setOnMouseClicked(new MausHandler());
		zeichne();
	}

	/** zeichnet das Spielfeld */
	private void zeichne() {
		gc.setFill(Color.WHITE);
		gc.fillRect(0, 0, getWidth(), getHeight());
		zeichneBilder();
		zeichneRaster();
	}
	
	/** zeichnet die Bilder, falls vorhanden, sonst nur die ID-Werte der Felder
	 *  als Zahlen */
	private void zeichneBilder() {
		if ( mf.getKontroller().hatBilder()) {
			for(byte x = 0; x < mf.getKontroller().getFeldmax(); x++) {
				for(byte y = 0; y < mf.getKontroller().getFeldmax(); y++) {
					try {
						if ( mf.getKontroller().getFeldPosition(x, y).getKarteVonFeld() != null && !mf.getKontroller().getFeldPosition(x, y).getKarteVonFeld().istAufgedeckt()) {
							gc.setFill(Color.LIGHTGRAY);
							gc.fillRect(x*ZELLE, y*ZELLE, ZELLE, ZELLE);
						} else if (mf.getKontroller().getFeldPosition(x, y).getKarteVonFeld() != null && mf.getKontroller().getFeldPosition(x, y).getKarteVonFeld().istAufgedeckt()) {
							gc.drawImage(mf.getKontroller().getFeldPosition(x, y).getKarteVonFeld().getBild(), x*ZELLE, y*ZELLE, ZELLE, ZELLE);
						}
					} catch (FeldException e) { }
				}
			}
		} else {
			for(byte x = 0; x < mf.getKontroller().getFeldmax(); x++) {
				for(byte y = 0; y < mf.getKontroller().getFeldmax(); y++) {
					try {
						if ( mf.getKontroller().getFeldPosition(x, y).getKarteVonFeld() != null && !mf.getKontroller().getFeldPosition(x, y).getKarteVonFeld().istAufgedeckt()) {
							gc.setFill(Color.LIGHTGRAY);
							gc.fillRect(x*ZELLE,y*ZELLE,ZELLE,ZELLE);
						} else if (mf.getKontroller().getFeldPosition(x, y).getKarteVonFeld() != null && mf.getKontroller().getFeldPosition(x, y).getKarteVonFeld().istAufgedeckt()) {
							gc.setFill(Color.WHITE);
							gc.strokeText(""+mf.getKontroller().getFeldPosition(x, y).getKarteVonFeld().getID(), x*ZELLE+ ZELLE/2, y*ZELLE + ZELLE/2);
						}
					} catch (FeldException e) { }
				}
			}
		}
	}

	/** zeichnet das Raster der Zellen als schwarzes Quadrat */
	private void zeichneRaster() {
		gc.setStroke(Color.BLACK);
		for(byte x = 0; x < mf.getKontroller().getFeldmax(); x++) {
			for(byte y = 0; y < mf.getKontroller().getFeldmax(); y++) {
				gc.strokeRect(x*ZELLE, y*ZELLE,ZELLE,ZELLE);
			}
		}
	}
	
	/**
	 * diese Funktion simuliert eine ganze Spielrunde, indem zuerst der Mausklick
	 * analysiert wird. Es wird ermittelt welche Zelle angeklickt wurde. Anschliessend
	 * wird der Spielerstatus aktualisiert (Punkte, Reihenfolge) und ueberprueft, ob
	 * das aktuelle Spiel zuende ist
	 * @param zx zelle X
	 * @param zy zelle Y
	 */
	private void runde(byte zx, byte zy) {
		mf.getKontroller().analysiereKlick(zx, zy);
		mf.setLabelText(mf.getKontroller().getAktSpieler().getID(), mf.getKontroller().getAktSpieler().getPunkte());
		mf.setLabelAktiverSpieler(mf.getKontroller().getAktSpieler().getID());
		mf.neuesSpiel();
		zeichne();
	}
	
	private class MausHandler implements EventHandler<MouseEvent> {

		@Override
		public void handle(MouseEvent me) {
			short px = (short) me.getX();
			short py = (short) me.getY();
			byte zx = (byte) (px / ZELLE);
			byte zy = (byte) (py / ZELLE);
			runde(zx,zy);
 		}
	}

}
