package root;

import java.util.ArrayList;
import java.util.List;

import ausnahmen.FeldException;
import javafx.scene.image.Image;

/**
 * Diese Klasse dient der Kommunikation zwischen den Spielern und den Feldern
 * @author Sebastian
 */
public class Kontroller {

	private Spieler[] spieler;
	private List<Karte> karten;
	private List<Image> bilder;
	private Karte eins, zwei;
	private final byte FELDMAX, DUMMY = -1;
	private byte feldEinsX, feldEinsY, feldZweiX, feldZweiY;
	private boolean karteEins, karteZwei, spielAktiv;
	private Feld[][] spielfeld;
	
	/**
	 * Der Konstruktor erhaelt die Kartenbilder von der Startklasse, sowie die Anzahl der Spieler
	 * und Kartenpaare, sowie wie gross das Spielfeld sein muss
	 * @param kartenbilder Bilder als Image
	 * @param anzahlSpieler Teilnehmer
	 * @param anzahlPaare Kartenpaare
	 * @param max Spielfeldgroesse
	 */
	public Kontroller(List<Image> kartenbilder, byte anzahlSpieler, byte anzahlPaare, byte max) {
		FELDMAX = max;
		bilder = kartenbilder;
		karteEins = karteZwei = false;
		spielAktiv = true;
		
		/* erzeuge Spielfeld */
		spielfeld = new Feld[FELDMAX][FELDMAX];
		for(int x = 0; x < FELDMAX; x++) {
			for(int y = 0; y < FELDMAX; y++) {
				spielfeld[x][y] = new Feld((byte) x,(byte) y);
			}
		}
		
		/* erzeuge Spieler */
		spieler = new Spieler[anzahlSpieler];
		for(byte s = 0; s < anzahlSpieler; s++) {
			spieler[s] = new Spieler(s);
		}
		spieler[0].setAmZug(true);
		
		/* erzeuge Karten */
		karten = new ArrayList<Karte>();
		for(byte i = 0; i < anzahlPaare; i++) {
			karten.add(new Karte((byte) i));
			karten.add(new Karte((byte) i));
		}
		
		/* Karte ohne Partner */
		karten.add(new Karte((byte) DUMMY));
		
		if ( !bilder.isEmpty()) {
			ordneKartenBilderZu();
		}
		mischeKarten(karten);
		verteileKartenAufFelder(karten);
	}
	
	/** Ordnet den Karten Bilder zu, falls vorhanden */
	private void ordneKartenBilderZu() {
		/* Mische die Bilder */
		byte mischen = 5;
		while(mischen > 0) {
			for(byte m = 0; m < bilder.size(); m++) {
				byte zufall = (byte) (Math.random() * bilder.size());
				Image temp = bilder.get(zufall);
				bilder.remove(temp);
				bilder.add(m,temp);
			}
			mischen--;
		}
		
		/* Karten Bilder zuordnen */
		byte c = 0;
		for(byte b = 1; b < karten.size() - 1; b += 2) {
			karten.get(b).setBild(bilder.get(c));
			karten.get(b-1).setBild(bilder.get(c));
			c++;
		}
		
		/* Kartenbild ohne Partner */
		karten.get(karten.size()-1).setBild(bilder.get(bilder.size()-1));
	}

	/**
	 * mischt die Memory-Karten
	 * @param karten Liste voller Karten
	 */
	private void mischeKarten(List<Karte> karten) {
		byte mischen = 5;
		while(mischen > 0) {
			for(byte i = 0; i < karten.size(); i++) {
				byte zufall = (byte) (Math.random() * karten.size());
				Karte temp = karten.get(zufall);
				karten.remove(temp);
				karten.add(temp);
			}
			mischen--;
		}
	}
	
	/**
	 * verteilt alle Karten auf die erzeugten Felder
	 * @param karten Memory-Karten fuer die Felder
	 */
	private void verteileKartenAufFelder(List<Karte> karten) {
		byte zaehler = (byte) (karten.size() - 1);
		for(int x = 0; x < FELDMAX; x++) {
			for(int y = 0; y < FELDMAX; y++) {
				spielfeld[x][y].setKarteAufFeld(karten.get(zaehler));
				karten.remove(karten.get(zaehler));
				if(zaehler == 0) {
					return;
				}
				zaehler--;
			}
		}
	}
	
	/** startet eine neue Spielrunde */
	public void neueSpielrunde() {
		spielAktiv = true;
		for(Spieler s : spieler) {
			karten.addAll(s.getSpielerkarten());
			s.setzeSpielerZurueck();
		}
		spieler[0].setAmZug(true);
		karten.add(new Karte((byte) DUMMY));
		karten.get(karten.size()-1).setBild(bilder.get(bilder.size()-1));
		mischeKarten(karten);
		verteileKartenAufFelder(karten);
	}
	
	/** kuemmert sich um den Spielerwechsel */
	private void spielerwechsel() {
		if ( getAktSpieler().getID() == spieler.length - 1) {
			getAktSpieler().setAmZug(false);
			setAktSpieler((byte) 0);
		} else {
			Spieler temp = getAktSpieler();
			setAktSpieler((byte) (getAktSpieler().getID() + 1));
			temp.setAmZug(false);
		}
	}

	/**
	 * deckt eine Karte auf
	 * @param x zelle X
	 * @param y zelle Y
	 * @return aufgedeckte Karte
	 */
	private Karte deckeKarteEinsAuf(byte x, byte y) {
		Karte eins = null;
		try {
			eins = getFeldPosition(x,y).getKarteVonFeld();
			getFeldPosition(x,y).getKarteVonFeld().deckeKarteAuf(true);
			karteEins = true;
			feldEinsX = x;
			feldEinsY = y;
			return eins;
		} catch (FeldException e) {
			karteEins = false;
			return null;
		}
	}
	
	/**
	 * deckt eine weitere Karte auf
	 * @param x zelle X
	 * @param y zelle Y
	 * @return aufgedeckte Karte
	 */
	private Karte deckeKarteZweiAuf(byte x, byte y) {
		Karte zwei = null;
		try {
			zwei = getFeldPosition(x,y).getKarteVonFeld();
			getFeldPosition(x,y).getKarteVonFeld().deckeKarteAuf(true);
			karteZwei = true;
			feldZweiX = x;
			feldZweiY = y;
			return zwei;
		} catch (FeldException e) {
			karteZwei = false;
			return null;
		}
	}
	
	/** prueft, ob die Karten gleich sind */
	public void kartenpruefung() {
		boolean gleich = Karte.istGleich(eins,zwei);
		if ( gleich ) {
			karteEins = karteZwei = false;
			getAktSpieler().addKarte(eins);
			getAktSpieler().addKarte(zwei);
			getAktSpieler().erhoehePunkt();
			try {
				getFeldPosition(feldEinsX,feldEinsY).getKarteVonFeld().deckeKarteAuf(false);
				getFeldPosition(feldZweiX,feldZweiY).getKarteVonFeld().deckeKarteAuf(false);
				getFeldPosition(feldEinsX,feldEinsY).entferneKarteAusFeld();
				getFeldPosition(feldZweiX,feldZweiY).entferneKarteAusFeld();
				eins = null;
				zwei = null;
			} catch (FeldException e) { }
		} else {
			eins = null;
			zwei = null;
			karteEins = karteZwei = false;
			try {
				getFeldPosition(feldEinsX,feldEinsY).getKarteVonFeld().deckeKarteAuf(false);
				getFeldPosition(feldZweiX,feldZweiY).getKarteVonFeld().deckeKarteAuf(false);
				spielerwechsel();
			} catch (FeldException e) { }
		}
	}

	/**
	 * gibt die Ergebnisverkuendung zurueck welcher Spieler gewonnen hat
	 * @return ergebnis als String
	 */
	public String ergebnisverkuendung() {
		Spieler gewinner = null;
		for(byte i = 1; i < spieler.length; i++) {
			if ( spieler[i].getPunkte() < spieler[i-1].getPunkte()) {
				gewinner = spieler[i-1];
			} else {
				gewinner = spieler[i];
			}
		}
		
		String ergebnis = "Spieler "+(gewinner.getID()+1)+" hat mit "+gewinner.getPunkte()+" Punkten gewonnen.";
		return ergebnis;
	}
	
	/**
	 * prueft, ob das aktuelle Spiel zuende ist
	 * @return true, Spiel vorbei, false sonst
	 */
	private boolean spielende() {
		for(byte x = 0; x < FELDMAX; x++) {
			for(byte y = 0; y < FELDMAX; y++) {
				try {
					if ( getFeldPosition(x,y).getKarteVonFeld() != null && getFeldPosition(x,y).getKarteVonFeld().getID() >= 0 ) {
						return false;
					}
				} catch (FeldException e) { }
			}
		}		
		spielAktiv = false;
		return true;
	}
	
	/** entfernt die Memory-Karte die kein Partner hat */
	private void dummyEntfernen() {
		for(byte x = 0; x < FELDMAX; x++) {
			for(byte y = 0; y < FELDMAX; y++) {
				try {
					if ( getFeldPosition(x,y).getKarteVonFeld() != null && getFeldPosition(x,y).getKarteVonFeld().getID() == DUMMY ) {
						getFeldPosition(x,y).entferneKarteAusFeld();
						return;
					}
				} catch (FeldException e) { }
			}
		}	
	}
	
	/**
	 * uebergibt die Koordinaten die mit der Maus angeklickt wurden. So wird die
	 * Zelle ermittelt.
	 * @param zx zelle X
	 * @param zy zelle Y
	 */
	public void analysiereKlick(byte zx, byte zy) {
		if ( !karteEins ) {
			eins = deckeKarteEinsAuf(zx,zy);
		} else if ( !karteZwei ){
			Karte temp = deckeKarteZweiAuf(zx,zy);
			if ( temp != null && temp.equals(eins)) {
				karteZwei = false;
			} else {
				zwei = temp;
			}
		} else if ( karteEins && karteZwei) {
			kartenpruefung();
		}
		
		if (spielende()) {
			dummyEntfernen();
		}
	}
	
	/**
	 * uebergibt die Spieler ID welcher Spieler an der Reihe ist
	 * @param s_index spieler ID
	 */
	public void setAktSpieler(byte s_index) {
		spieler[(int) s_index].setAmZug(true);
	}
	
	/**
	 * ermittelt den aktuellen Spieler
	 * @return aktueller Spieler
	 */
	public Spieler getAktSpieler() {
		for(Spieler s : spieler) {
			if ( s.getAmZug()) {
				return s;
			}
		}
		return null;
	}
	
	/**
	 * gibt den Spielstatus zurueck, ob das Spiel noch im Gange ist
	 * @return true, Spiel ist noch nicht vorbei, false, Spiel ist vorbei
	 */
	public boolean spielStatus() {
		return spielAktiv;
	}
	
	/**
	 * prueft, ob Bilder eingeladen wurden sind
	 * @return true, es gibt genuegend Bilder sonst false
	 */
	public boolean hatBilder() {
		if ( bilder.isEmpty() ) {
			return false;
		} else {
			return true;
		}
	}
	
	/**
	 * gibt eine Referenz auf ein bestimmtes Feld zurueck
	 * @param x x-Koordinate
	 * @param y y-Koordinate
	 * @return Feld mit dem Punkt (x,y)
	 */
	public Feld getFeldPosition(byte x, byte y) {
		return spielfeld[x][y];
	}
	
	/**
	 * gibt die maximale Spielfeldgroesse zurueck
	 * @return maximale Spielfeldgroesse
	 */
	public byte getFeldmax() {
		return FELDMAX;
	}
}
